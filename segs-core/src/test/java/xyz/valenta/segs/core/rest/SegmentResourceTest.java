package xyz.valenta.segs.core.rest;

import io.restassured.response.Response;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.boot.test.autoconfigure.restdocs.AutoConfigureRestDocs;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import xyz.valenta.segs.core.app.SegsApplication;
import xyz.valenta.segs.dto.Segment;

import java.util.Arrays;
import java.util.List;

import static io.restassured.RestAssured.given;

@ExtendWith(SpringExtension.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT, classes = SegsApplication.class)
public class SegmentResourceTest {
  private static final Segment EXPECTED_SEGMENT_1 = Segment.builder()
    .id(1)
    .name("foo")
    .build();
  private static final Segment EXPECTED_SEGMENT_2 = Segment.builder()
    .id(2)
    .name("bar")
    .build();

  @LocalServerPort
  protected int serverPort;

  @Test
  void returnsOneMatchingSegment() {
    Response response = given().port(serverPort)
      .when()
      .get("/segments/{ids}", "1,3");

    Segment[] segments = response.getBody().as(Segment[].class);
    Assertions.assertEquals(1, segments.length);
    Assertions.assertEquals(EXPECTED_SEGMENT_1, segments[0]);
  }

  @Test
  void returnsAllMatchingSegments() {
    Response response = given().port(serverPort)
      .when()
      .get("/segments/{ids}", "1,2");

    List<Segment> segments = Arrays.asList(response.getBody().as(Segment[].class));
    Assertions.assertAll(
      () -> Assertions.assertEquals(2, segments.size()),
      () -> Assertions.assertTrue(segments.contains(EXPECTED_SEGMENT_1)),
      () -> Assertions.assertTrue(segments.contains(EXPECTED_SEGMENT_2))
    );
  }

  @Test
  void returnsEmptyArrayWhenNoMatchingSegments() {
    Response response = given().port(serverPort)
      .when()
      .get("/segments/{ids}", "5,6");

    Segment[] segments = response.getBody().as(Segment[].class);
    Assertions.assertEquals(0, segments.length);
  }
}
