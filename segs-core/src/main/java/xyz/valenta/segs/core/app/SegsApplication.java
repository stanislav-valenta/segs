package xyz.valenta.segs.core.app;


import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication(scanBasePackages = {"xyz.valenta.segs.core.rest", "xyz.valenta.segs.core.config"})
public class SegsApplication {
  public static void main(String[] args) {
    SpringApplication.run(SegsApplication.class, args);
  }
}
