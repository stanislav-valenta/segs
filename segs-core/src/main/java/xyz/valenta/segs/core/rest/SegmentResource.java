package xyz.valenta.segs.core.rest;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.Authorization;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import xyz.valenta.segs.dto.Segment;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@RestController
@Api(value="Segments", description="Operations for segments")
public class SegmentResource {
  @ApiOperation(value  = "Get list of segments based on ids")
  @RequestMapping(value = "/segments/{ids}", method = RequestMethod.GET, produces = "application/json")
  public List<Segment> segments(
    @ApiParam(value = "comma separated values, like '2,3,4'", example = "2,3,4") @PathVariable Long[] ids
  ) {

    return Stream.of(
      Segment.builder()
        .id(1)
        .name("foo")
        .build(),
      Segment.builder()
        .id(2)
        .name("bar")
        .build()
    )
      .filter(s -> Arrays.asList(ids).contains(s.getId()))
      .collect(Collectors.toList());
  }

  @ApiOperation(value  = "Get list of segments based on ids")
  @RequestMapping(value = "/segments/search", method = RequestMethod.GET, produces = "application/json")
  public List<Segment> searchSegments(
    @ApiParam(value = "array of segments") @RequestBody Segment[] segments
  ) {

    return Arrays.asList(
      Segment.builder()
        .id(1)
        .name("foo")
        .build(),
      Segment.builder()
        .id(2)
        .name("bar")
        .build()
    );
  }
}


