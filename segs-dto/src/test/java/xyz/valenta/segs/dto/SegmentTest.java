package xyz.valenta.segs.dto;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertAll;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

class SegmentTest {
  @Test
  void immutableInstanceCreatedViaBuilder() {
    Segment segment = Segment.builder()
      .id(1)
      .name("name")
      .build();

    assertNotNull(segment);
    assertEquals("name", segment.getName());
    assertEquals(1L, segment.getId());
  }

  @Test
  void toStringCreated() {
    Segment segment = Segment.builder()
      .id(1)
      .name("name")
      .build();

    assertEquals("Segment(id=1, name=name)", segment.toString());
  }

  @Test
  void hashcodeDiffersForDifferentIds() {
    Segment segment1 = Segment.builder()
      .id(1)
      .name("name")
      .build();

    Segment segment2 = Segment.builder()
      .id(2)
      .name("name")
      .build();

    assertNotEquals(segment1.hashCode(), segment2.hashCode());
  }

  @Test
  void hashcodeDiffersForDifferentNames() {
    Segment segment1 = Segment.builder()
      .id(1)
      .name("name1")
      .build();

    Segment segment2 = Segment.builder()
      .id(1)
      .name("name2")
      .build();

    assertNotEquals(segment1.hashCode(), segment2.hashCode());
  }

  @Test
  void hashcodeEqualsForSameStates() {
    Segment segment1 = Segment.builder()
      .id(1)
      .name("name")
      .build();

    Segment segment2 = Segment.builder()
      .id(1)
      .name("name")
      .build();

    assertAll(
      () -> assertEquals(segment1, segment2),
      () -> assertEquals(segment1.hashCode(), segment2.hashCode())
    );
  }

}
