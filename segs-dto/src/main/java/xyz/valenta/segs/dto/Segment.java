package xyz.valenta.segs.dto;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonPOJOBuilder;
import lombok.Builder;
import lombok.Value;

@Builder(builderClassName = "Builder")
@JsonDeserialize(builder = Segment.Builder.class)
@Value
public class Segment   {
  private  long id;
  private  String name;

  @JsonPOJOBuilder(withPrefix = "")
  public static class Builder {

  }
}
